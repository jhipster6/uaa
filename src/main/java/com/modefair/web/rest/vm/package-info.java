/**
 * View Models used by Spring MVC REST controllers.
 */
package com.modefair.web.rest.vm;
